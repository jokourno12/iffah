#IFFAH
- Iffah is a taaruf application to bring together ikhwan and akhwat to get married.

##AUTHENTICATION
- Token with role

##AUTHORIZATION
- Admin, Ikhwan, and Akhwat

##DATABASE
- MongoDB

#ALGORITMA
1. Install Node.js:
    - curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    - or
    - wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    - export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    - source ~/.bashrc
    - nvm -v
    - nvm install latest
    - If node with version: nvm install vX.Y.Z
2. Install MongoDB on Linux:
    - sudo su
    - apt-get update
    - mongod --version
    - wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | sudo apt-key add -
    - echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/6.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-6.0.list
    - apt-get update
    - apt-get upgrade -y
    - sudo apt install mongodb-org
    - sudo systemctl enable mongod
    - sudo service mongod start
    - sudo service mongod status
3. Install MongoDB Compass (optional):
    - wget https://downloads.mongodb.com/compass/mongodb-compass_1.40.4_amd64.deb
    - sudo dpkg -i mongodb-compass_1.40.4_amd64.deb
    - mongodb-compass
4. Create new project:
    - mkdir iffah
    - cd ./iffah
    - npm init
    - npm install bcryptjs
    - npm install express
    - npm install jsonwebtoken
    - npm install mongodb
    - npm install mongoose
5. Directory structure:
    - connection
    - controllers
    - midlleware
    - models
    - public
    - routes
    - services
    - tests
    - utils