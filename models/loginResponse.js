const loginResponse = (role, token = null) => {
    return{
        role: role,
        token: token
    };
};

module.exports = loginResponse;