const User = require("../models/userModel");

exports.getAllAkhwat = async (req, res) => {
    try{
        const users = await User.find({role: "akhwat"});
        res.json(users);
    }catch(error){
        console.error("Error fetching akhwat:", error);
        res.status(500).json({message: "Internal server error"});
    };
};