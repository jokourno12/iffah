const User = require("../models/userModel");

exports.getAllIkhwan = async (req, res) => {
    try{
        const users = await User.find({role: "ikhwan"});
        res.json(users);
    }catch(error){
        console.error("Error fetching ikhwan:", error);
        res.status(500).json({message: "Internal server error"});
    };
};