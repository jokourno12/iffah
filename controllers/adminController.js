const User = require("../models/userModel");

exports.getAllUsers = async (req, res) => {
    try{
        const users = await User.find();
        res.json(users);
    }catch(error){
        console.error("Error fetching users:", error);
        res.status(500).json({ message: "Internal server error" });
    };
};

exports.updateUser = async (req, res) => {
    try{
        const userId = req.params.userId;
        const { name, email, password } = req.body;

        const existingUser = await User.findById(userId);
        if(!existingUser){
            return res.status(404).json({message: "User not found"});
        }

        existingUser.name = name;
        existingUser.email = email;
        existingUser.password = password;

        const updatedUser = await existingUser.save();
        res.json(updatedUser);
    }catch(error){
        console.error("Error updating user:", error);
        res.status(500).json({message: "Internal server error"});
    };
};

exports.deleteUser = async (req, res) => {
    try{
        const userId = req.params.userId;

        const user = await User.findById(userId);
        if(!user){
            return res.status(404).json({message: "User not found"});
        }

        await User.findByIdAndDelete(userId);

        res.json({message: "User deleted successfully"});
    }catch(error){
        console.error("Error deleting user:", error);
        res.status(500).json({message: "Internal server error"});
    }
};