const User = require("../models/userModel");
const commonResponse = require("../models/commonResponse");
const loginResponse = require("../models/loginResponse");
const http = require("http");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.registerAdmin = async (req, res) => {
    try{
        const { name, email, password } = req.body;
        const role = "admin";

        const hashedPassword = await bcrypt.hash(password, 12);

        const newUser = new User({ name, email, password: hashedPassword, role });
        const savedUser = await newUser.save();
        res.status(201).json(commonResponse(http.STATUS_CODES[201], "Register admin successfully", savedUser));
    }catch(error){
        console.error("Error creating user:", error);
        res.status(500).json({message: "Internal server error"});
    }
};

exports.registerIkhwan = async (req, res) => {
    try{
        const { name, email, password } = req.body;
        const role = "ikhwan";

        const hashedPassword = await bcrypt.hash(password, 12);

        const newUser = new User({ name, email, password: hashedPassword, role });
        const savedUser = await newUser.save();
        res.status(201).json(commonResponse(http.STATUS_CODES[201], "Register ikhwan successfully", savedUser));
    }catch(error){
        console.error("Error creating user:", error);
        res.status(500).json({message: "Internal server error"});
    }
};

exports.registerAkhwat = async (req, res) => {
    try{
        const { name, email, password } = req.body;
        const role = "akhwat";

        const hashedPassword = await bcrypt.hash(password, 12);

        const newUser = new User({ name, email, password: hashedPassword, role });
        const savedUser = await newUser.save();
        res.status(201).json(commonResponse(http.STATUS_CODES[201], "Register akhwat successfully", savedUser));
    }catch(error){
        console.error("Error creating user:", error);
        res.status(500).json({message: "Internal server error"});
    }
};

exports.login = async (req, res) => {
    try{
        const { email, password } = req.body;
        const user = await User.findOne({ email: email }, {password: 1, role: 1} );

        if(!user){
            return res.status(400).json({ message: "User not found" })
        };

        const isPasswordValid = await bcrypt.compare(password, user.password);

        if(!isPasswordValid){
            return res.status(400).json({ message: "Invalid password" });
        };

        const role = user.role;
        const token = jwt.sign({email: user.email, role: user.role}, "rahayu", {expiresIn: "1h"});

        res.status(201).json(loginResponse(role, token));
    }catch(error){
        console.error("Error logging in:", error);
        res.status(500).json({message: "Internal server error"});
    }
};