const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
    const token = req.headers.authorization;
    console.log(token);
    if(!token){
        return res.status(403).json({ message: "No token provided" });
    };

    jwt.verify(token, "rahayu", (err, decoded) => {
        if(err){
            return res.status(401).json({message: "Failed to authenticate token"});
        };

        req.decoded = decoded;
        next();
    });
};

const checkRole = (role) => {
    return (req, res, next) => {
        const token = req.headers.authorization;
        jwt.verify(token, "rahayu", (err, decode) => {
            if(decode.role == role){
                next();
            }else{
                return res.status(403).json({ message: "Not authorized" });
            };
        });
    };
};

module.exports = { verifyToken, checkRole };