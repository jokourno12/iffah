const express = require("express");
const router = express.Router();
const authController = require("../controllers/authController");
const adminController = require("../controllers/adminController");
const ikhwanController = require("../controllers/ikhwanController");
const akhwatController = require("../controllers/akhwatController ");
const { checkRole } = require("../middleware/jwtVerify");

//Authentication register
router.post('/register/admin', authController.registerAdmin);
router.post('/register/ikhwan', authController.registerIkhwan);
router.post('/register/akhwat', authController.registerAkhwat);

//Authetntication login
router.post('/login', authController.login);

// Authorization Admin
router.get('/admin', checkRole("admin"), adminController.getAllUsers);
router.put('/admin/:userId', checkRole("admin"), adminController.updateUser);
router.delete('/admin/:userId', checkRole("admin"), adminController.deleteUser);

// Authorization Ikhwan
router.get('/akhwat', checkRole("ikhwan"), ikhwanController.getAllAkhwat);

// Authorization Akhwat
router.get('/ikhwan', checkRole("akhwat"), akhwatController.getAllIkhwan);

module.exports = router;