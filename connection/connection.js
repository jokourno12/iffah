const mongoose = require("mongoose");

const mongodb_uri = "mongodb://localhost:27017/iffah_db";

async function connectToDatabase(){
    try{
        await mongoose.connect(mongodb_uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("Connected to database successfully");
    }catch(error){
        console.error("Could not connect to database:", error.message);
    };
};

module.exports = connectToDatabase;